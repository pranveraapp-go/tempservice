package main

import (
	"flag"
	"fmt"
	"tempService/models"
	"tempService/printer"
	"tempService/server"
)

func main2() {

	fmt.Println("Welcome")
	beachReady := flag.Bool("beach", false, "Display only beach ready")
	skiReady := flag.Bool("ski", false, "Display only ski ready")
	month := flag.Int("month", 0, "Lookup for destination month [1,12]")
	name := flag.String("name", "", "Lookup for destination by name")
	flag.Parse()
	cities, err := models.NewCities(server.NewConnection())
	cq, err := models.NewQuery(*beachReady, *skiReady, *month, *name)
	if err != nil {
		fmt.Println("Fatal error, ", err)
		return
	}
	p := printer.New()
	defer p.Cleanup()
	p.CityHeader()

	cs := cities.Filter(cq)

	for _, c := range cs {
		p.CityDetails(c, cq)
	}
}
