package server_test

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"os"
	"tempService/server"
	"testing"
)

var a server.App

func TestMain(m *testing.M) {
	a = server.App{}
	a.Initialize()

	ensureTableExsists()
	code := m.Run()

	clearTables()
	os.Exit(code)
}

func executeRequest(req *http.Request) *httptest.ResponseRecorder {
	rr := httptest.NewRecorder()
	a.Router.ServeHTTP(rr, req)
	return rr
}
func checkResponseCode(t *testing.T, expected, actual int) {
	if expected != actual {
		t.Errorf("Expected response code %d. Got %d\n", expected, actual)
	}
}

func TestGetNonExsistentCity(t *testing.T) {
	clearTables()
	req, _ := http.NewRequest("GET", "/city/111", nil)
	resp := executeRequest(req)
	checkResponseCode(t, http.StatusInternalServerError, resp.Code)

	var m map[string]string
	json.Unmarshal(resp.Body.Bytes(), &m)
	if m["error"] != "sql: no rows in result set" {
		t.Errorf("Expected the 'error' key of the response to be set to 'sql: no rows in result set'"+" Got '%s'", m["error"])
	}
}

func TestCreateCity(t *testing.T) {
	clearTables()
	payload := []byte(`{
	"id":1,
	"cap":"ABC",
	"name":"Genova",
	"has_beach":false,
	"has_mountain":true,
	"temperature":[{
	"city_id":1,
	"value":16
}]
}
`)
	req, _ := http.NewRequest("POST", "/cities", bytes.NewBuffer(payload))
	resp := executeRequest(req)
	checkResponseCode(t, http.StatusOK, resp.Code)

	var m server.RespCities
	json.Unmarshal(resp.Body.Bytes(), &m)
	var temps []server.Temps = m.Temperature
	if m.Cap != "ABC" {
		t.Errorf("Expected cap to be 'ABC'. Got '%v'", m.Cap)
	}
	if m.Name != "Genova" {
		t.Errorf("Expected cap to be 'Genova'. Got '%v'", m.Name)
	}
	if m.HasBeach != false {
		t.Errorf("Expected cap to be 'false'. Got '%v'", m.HasBeach)
	}
	if m.HasMountain != true {
		t.Errorf("Expected cap to be 'true'. Got '%v'", m.HasMountain)
	}
	// testing the first Temps on the slice
	if temps[0].Id != 1.0 {
		t.Errorf("Expected Id to be '1'. Got '%v'", temps[0].Id)
	}
	if temps[0].CityId != 1.0 {
		t.Errorf("Expected CityId to be '1'. Got '%v'", temps[0].CityId)
	}
	if temps[0].Value != 16.0 {
		t.Errorf("Expected Value to be '16'. Got '%v'", temps[0].Value)
	}
}

func TestGetCity(t *testing.T) {
	clearTables()
	persistCity()
	for i := 0; i < 12; i++ {
		ff := float64(i)
		persistTemp(1, ff+0.5)
	}

	req, _ := http.NewRequest("GET", "/city/1", nil)
	resp := executeRequest(req)
	checkResponseCode(t, http.StatusOK, resp.Code)

	var m server.RespCities
	json.Unmarshal(resp.Body.Bytes(), &m)
	var temps = m.Temperature
	if m.Cap != "tir" {
		t.Errorf("Expected cap to be 'tir'. Got '%v'", m.Cap)
	}
	if m.Name != "Tirana Albania" {
		t.Errorf("Expected cap to be 'Tirana Albania'. Got '%v'", m.Name)
	}
	if m.HasBeach != false {
		t.Errorf("Expected cap to be 'false'. Got '%v'", m.HasBeach)
	}
	if m.HasMountain != true {
		t.Errorf("Expected cap to be 'true'. Got '%v'", m.HasMountain)
	}
	// testing the first Temps on the slice
	if temps[1].Id != 2.0 {
		t.Errorf("Expected Id to be '1'. Got '%v'", temps[1].Id)
	}
	if temps[1].CityId != 1.0 {
		t.Errorf("Expected CityId to be '1'. Got '%v'", temps[0].CityId)
	}
	if temps[1].Value != 1.5 {
		t.Errorf("Expected Value to be '16'. Got '%v'", temps[1].Value)
	}
}

func TestGetCities(t *testing.T) {
	clearTables()
	persistCity()
	for i := 0; i < 12; i++ {
		ff := float64(i)
		persistTemp(1, ff+1.5)
	}

	req, _ := http.NewRequest("GET", "/cities", nil)
	resp := executeRequest(req)
	checkResponseCode(t, http.StatusOK, resp.Code)

	var m []server.RespCities
	json.Unmarshal(resp.Body.Bytes(), &m)
	var temps = m[0].Temperature
	if m[0].Cap != "tir" {
		t.Errorf("Expected cap to be 'tir'. Got '%v'", m[0].Cap)
	}
	if m[0].Name != "Tirana Albania" {
		t.Errorf("Expected cap to be 'Tirana Albania'. Got '%v'", m[0].Name)
	}
	if m[0].HasBeach != false {
		t.Errorf("Expected cap to be 'false'. Got '%v'", m[0].HasBeach)
	}
	if m[0].HasMountain != true {
		t.Errorf("Expected cap to be 'true'. Got '%v'", m[0].HasMountain)
	}
	// testing the first Temps on the slice
	if temps[1].Id != 2.0 {
		t.Errorf("Expected Id to be '1'. Got '%v'", temps[1].Id)
	}
	if temps[1].CityId != 1.0 {
		t.Errorf("Expected CityId to be '1'. Got '%v'", temps[1].CityId)
	}
	if temps[1].Value != 2.5 {
		t.Errorf("Expected Value to be '2.5'. Got '%v'", temps[1].Value)
	}
}

func TestGetNonExsistentTemperature(t *testing.T) {
	clearTables()
	req, _ := http.NewRequest("GET", "/temp/111", nil)
	resp := executeRequest(req)
	checkResponseCode(t, http.StatusInternalServerError, resp.Code)

	var m map[string]string
	json.Unmarshal(resp.Body.Bytes(), &m)
	if m["error"] != "sql: no rows in result set" {
		t.Errorf("Expected the 'error' key of the response to be set to 'sql: no rows in result set'"+" Got '%s'", m["error"])
	}
}

func TestCreateTemps(t *testing.T) {
	clearTables()
	payload := []byte(`{
	"city_id":1,
	"value":15}
`)
	req, _ := http.NewRequest("POST", "/temps", bytes.NewBuffer(payload))
	resp := executeRequest(req)
	checkResponseCode(t, http.StatusOK, resp.Code)

	var m map[string]interface{}
	json.Unmarshal(resp.Body.Bytes(), &m)
	if m["id"] != 1.0 {
		t.Errorf("Expected id to be '1'. Got '%v'", m["id"])
	}
	if m["city_id"] != 1.0 {
		t.Errorf("Expected city_id to be '1'. Got '%v'", m["city_id"])
	}
	if m["value"] != 15.0 {
		t.Errorf("Expected value to be '15'. Got '%v'", m["value"])
	}

}

func TestGetTemps(t *testing.T) {
	clearTables()
	persistTemp(1, 12.5)
	req, _ := http.NewRequest("GET", "/temp/1", nil)
	resp := executeRequest(req)
	checkResponseCode(t, http.StatusOK, resp.Code)

	var m map[string]interface{}
	json.Unmarshal(resp.Body.Bytes(), &m)
	if m["city_id"] != 1.0 {
		t.Errorf("Expected city_id to be '1'. Got '%v'", m["city_id"])
	}
	if m["value"] != 12.5 {
		t.Errorf("Expected value to be '12.5'. Got '%v'", m["value"])
	}

}
