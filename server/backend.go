package server

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	_ "github.com/mattn/go-sqlite3"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"
)

type App struct {
	DB     *sql.DB
	Port   string
	Router *mux.Router
}

func (a *App) Initialize() {
	DB, err := sql.Open("sqlite3", "./practiceit.db")
	if err != nil {
		log.Fatal(err.Error())
	}

	a.DB = DB
	a.Router = mux.NewRouter()
	a.InitializeRoutes()
}

func (a *App) Run() {
	fmt.Println("Server started and listening on port ", a.Port)
	log.Fatal(http.ListenAndServe(a.Port, a.Router))
}

func (a *App) InitializeRoutes() {
	a.Router.HandleFunc("/city/{id}", a.fetchCity).Methods("GET")
	a.Router.HandleFunc("/cities", a.newCity).Methods("POST")
	a.Router.HandleFunc("/cities", a.allCities).Methods("GET")
	a.Router.HandleFunc("/temp/{id}", a.fetchTemp).Methods("GET")
	a.Router.HandleFunc("/temps", a.newTemp).Methods("POST")
}

func (a *App) allCities(w http.ResponseWriter, r *http.Request) {
	cities, err := GetCities(a.DB)
	if err != nil {
		fmt.Printf("allCities error: %s\n", err)
		respondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}
	responseWithJSON(w, http.StatusOK, cities)
}

func (a *App) newTemp(w http.ResponseWriter, r *http.Request) {
	reqBody, _ := ioutil.ReadAll(r.Body)
	var t Temps
	json.Unmarshal(reqBody, &t)
	t, err := CreateATemp(t, a.DB)
	if err != nil {
		fmt.Printf("newTemp error: %s\n", err)
		respondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}
	responseWithJSON(w, http.StatusOK, t)
}

func (a *App) newCity(w http.ResponseWriter, r *http.Request) {
	reqBody, _ := ioutil.ReadAll(r.Body)
	var rc RespCities
	json.Unmarshal(reqBody, &rc)
	rc, err := CreateACity(rc, a.DB)
	if err != nil {
		fmt.Printf("newCity error: %s\n", err)
		respondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}
	for i := 0; i < len(rc.Temperature); i++ {
		var updatedTemp Temps
		updatedTemp, err = CreateATemp(rc.Temperature[i], a.DB)
		rc.Temperature[i] = updatedTemp
		if err != nil {
			fmt.Printf("newTemp error: %s\n", err)
			respondWithError(w, http.StatusInternalServerError, err.Error())
			return
		}
	}
	responseWithJSON(w, http.StatusOK, rc)
}

func (a *App) fetchCity(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	var intId int
	id := vars["id"]

	intId, _ = strconv.Atoi(id)
	temp, err := GetCity(intId, a.DB)
	if err != nil {
		fmt.Printf("fetch City error: %s\n", err)
		respondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}
	responseWithJSON(w, http.StatusOK, temp)

}

func (a *App) fetchTemp(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	var intId int
	id := vars["id"]

	intId, _ = strconv.Atoi(id)
	temp, err := GetTemp(intId, a.DB)
	if err != nil {
		fmt.Printf("fetch Temps error: %s\n", err)
		respondWithError(w, http.StatusInternalServerError, err.Error())
		return
	}
	responseWithJSON(w, http.StatusOK, temp)

}

func respondWithError(w http.ResponseWriter, code int, message string) {
	responseWithJSON(w, code, map[string]string{"error": message})
}

func responseWithJSON(w http.ResponseWriter, code int, payload interface{}) {
	resp, _ := json.Marshal(payload)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	w.Write(resp)
}
