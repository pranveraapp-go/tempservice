package server

import (
	"database/sql"
)

type RespCities struct {
	Id          int     `json:"id"`
	Cap         string  `json:"cap"`
	Name        string  `json:"name"`
	HasBeach    bool    `json:"has_beach"`
	HasMountain bool    `json:"has_mountain"`
	Temperature []Temps `json:"temperature"`
}

type Temps struct {
	Id     int     `json:"id"`
	CityId int     `json:"city_id"`
	Value  float64 `json:"value"`
}

type readerNew struct {
	DB *sql.DB
}

func NewConnection() DataReader {
	a := App{}
	a.Initialize()
	return &readerNew{
		DB: a.DB,
	}
}

//go:generate mockgen -destination=../mocks/mock_responses.go -package=mocks tempService/server DataReader
type DataReader interface {
	ReadCities() ([]RespCities, error)
	GetDB() *sql.DB
}

func (r *readerNew) GetDB() *sql.DB {
	return r.DB
}

func (r *readerNew) ReadCities() ([]RespCities, error) {
	cities, err := GetCities(r.DB)
	if err != nil {
		return nil, err
	}
	return cities, nil
}

func GetTempsByCityId(db *sql.DB, cityId int) ([]Temps, error) {
	rows, err := db.Query("SELECT * FROM temps where city_id=?", cityId)
	if err != nil {
		return nil, err
	}

	defer rows.Close()

	temps := []Temps{}

	for rows.Next() {
		var tt Temps
		if err := rows.Scan(&tt.Id, &tt.CityId, &tt.Value); err != nil {
			return nil, err
		}

		temps = append(temps, tt)
	}
	return temps, err
}

func GetTemps(db *sql.DB) ([]Temps, error) {
	rows, err := db.Query("SELECT * FROM temps")
	if err != nil {
		return nil, err
	}

	defer rows.Close()

	var temps []Temps

	for rows.Next() {
		var tt Temps
		if err := rows.Scan(&tt.Id, &tt.CityId, &tt.Value); err != nil {
			return nil, err
		}

		temps = append(temps, tt)
	}
	return temps, err
}

func CreateATemp(t Temps, db *sql.DB) (Temps, error) {
	res, err := db.Exec("INSERT INTO temps(city_id, value) VALUES (?,?)", t.CityId, t.Value)
	if err != nil {
		return t, err
	}
	id, err := res.LastInsertId()
	if err != nil {
		return t, err
	}
	t.Id = int(id)
	return t, nil
}

func CreateACity(rc RespCities, db *sql.DB) (RespCities, error) {
	res, err := db.Exec("INSERT INTO cities(cap, name, has_beach, has_mountain) VALUES (?, ?, ?, ?)",
		rc.Cap, rc.Name, rc.HasBeach, rc.HasMountain)
	if err != nil {
		return rc, err
	}
	id, err := res.LastInsertId()
	if err != nil {
		return rc, err
	}
	rc.Id = int(id)
	return rc, nil
}

func GetTemp(id int, db *sql.DB) (Temps, error) {
	var temp Temps
	row := db.QueryRow("SELECT id, city_id, value FROM temps WHERE id = ?", id)
	err := row.Scan(&temp.Id, &temp.CityId, &temp.Value)
	if err != nil {
		return temp, err
	}
	return temp, nil
}

func GetCity(id int, db *sql.DB) (RespCities, error) {
	var c RespCities
	row := db.QueryRow("SELECT id, cap, name, has_beach, has_mountain value FROM cities WHERE id = ?", id)
	err := row.Scan(&c.Id, &c.Cap, &c.Name, &c.HasBeach, &c.HasMountain)
	if err != nil {
		return c, err
	}
	c.Temperature, err = GetTempsByCityId(db, c.Id)

	return c, nil
}

func GetCities(db *sql.DB) ([]RespCities, error) {
	rows, err := db.Query("SELECT * FROM cities")
	if err != nil {
		return nil, err
	}

	defer rows.Close()

	var cities []RespCities

	for rows.Next() {
		var c RespCities
		if err := rows.Scan(&c.Id, &c.Cap, &c.Name, &c.HasBeach, &c.HasMountain); err != nil {
			return nil, err
		}
		c.Temperature, err = GetTempsByCityId(db, c.Id)
		cities = append(cities, c)
	}
	return cities, err
}
