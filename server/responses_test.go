package server_test

import (
	"log"
	"tempService/server"
	"testing"
)

const tablesCreationQuery = `
CREATE  TABLE IF NOT EXISTS cities
(
    id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    cap VARCHER(25) NOT NULL,
    name VARCHAR(50) NOT NULL,
    has_beach BOOLEAN NOT NULL check ( has_beach in (0,1)),
    has_mountain BOOLEAN NOT NULL check ( cities.has_mountain in (0,1))
);

CREATE TABLE IF NOT EXISTS temps
(
    id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
	city_id INTEGER NOT NULL,
	value INTEGER NOT NULL,
	FOREIGN KEY (city_id) REFERENCES cities(id)
)
`

var dataReader = server.NewConnection()

func TestTemps(t *testing.T) {
	clearTables()
	persistCity()
	persistTemp(1, 12.5)

	temps, err := server.GetTemps(dataReader.GetDB())
	if err != nil {
		log.Fatalf("database error %v", err)
	}
	if temps[0].Value != 12.5 {
		t.Errorf("Expected name to be '12.5'. Got '%v'", temps[0].Value)
	}

}

func TestTempsById(t *testing.T) {

	clearTables()
	persistCity()
	persistTemp(1, 12.5)

	temps, err := server.GetTempsByCityId(dataReader.GetDB(), 1)
	if err != nil {
		log.Fatalf("database error %v", err)
	}
	if temps[0].Value != 12.5 {
		t.Errorf("Expected name to be '12.5'. Got '%v'", temps[0].Value)
	}

}

func persistTemp(c int, v float64) {
	_, err := a.DB.Exec("INSERT INTO  temps(city_id, value)"+
		" VALUES(?, ?)", c, v)
	if err != nil {
		log.Fatal(err)
	}
	return
}

func TestCities(t *testing.T) {
	clearTables()
	persistCity()
	cities, err := server.GetCities(dataReader.GetDB())

	if err != nil {
		log.Fatalf("database error %v", err)
	}

	if cities[0].Cap != "tir" {
		t.Errorf("Expected name to be 'tir'. Got '%v'", cities[0].Cap)
	}
	if cities[0].Name != "Tirana Albania" {
		t.Errorf("Expected name to be 'Tirana Albania'. Got '%v'", cities[0].Name)
	}
	if cities[0].HasBeach != false {
		t.Errorf("Expected name to be 'false'. Got '%v'", cities[0].HasBeach)
	}
	if cities[0].HasMountain != true {
		t.Errorf("Expected name to be 'true'. Got '%v'", cities[0].HasMountain)
	}

}

func clearTables() {
	a.DB.Exec("DELETE FROM cities")
	a.DB.Exec("DELETE FROM sqlite_sequence WHERE name = 'cities'")
	a.DB.Exec("DELETE FROM temps")
	a.DB.Exec("DELETE FROM sqlite_sequence WHERE name = 'temps'")
}

func ensureTableExsists() {
	if _, err := dataReader.GetDB().Exec(tablesCreationQuery); err != nil {
		log.Fatal(err)
	}
}

func persistCity() {
	_, err := a.DB.Exec("INSERT INTO  cities(cap, name, has_beach, has_mountain)"+
		" VALUES(?, ?, ?, ?)", "tir", "Tirana Albania", false, true)
	if err != nil {
		log.Fatal(err)
	}
	return
}
