module tempService

go 1.22.2

require (
	github.com/golang/mock v1.6.0
	github.com/gorilla/mux v1.8.1
	github.com/mattn/go-sqlite3 v1.14.22
)
