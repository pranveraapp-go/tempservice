package models_test

import (
	"fmt"
	"github.com/golang/mock/gomock"
	"tempService/mocks"
	"tempService/models"
	"tempService/server"
	"testing"
)

func TestFilter(t *testing.T) {
	ctrl := gomock.NewController(t)
	mockReader := mocks.NewMockDataReader(ctrl)

	tests := []struct {
		testName    string
		RespCitiess []server.RespCities
		query       models.CityQuery
		want        []server.RespCities
	}{
		{
			testName:    "list all",
			RespCitiess: []server.RespCities{beachReady1(), skiReady()},
			query:       createQuery(t, false, false, 1, ""),
			want:        []server.RespCities{beachReady1(), skiReady()},
		},
		{
			testName:    "beach only",
			RespCitiess: []server.RespCities{beachReady1(), beachReady2(), skiReady()},
			query:       createQuery(t, true, false, 2, ""),
			want:        []server.RespCities{beachReady1(), beachReady2()},
		},
		{
			testName:    "beach only but the month doesn't exists at one city",
			RespCitiess: []server.RespCities{beachReady1(), beachReady2(), skiReady()},
			query:       createQuery(t, true, false, 3, ""),
			want:        []server.RespCities{beachReady1()},
		},
	}

	for _, tt := range tests {
		t.Run(tt.testName, func(t *testing.T) {
			mockReader.EXPECT().ReadCities().Return(tt.RespCitiess, nil).Times(1)
			cities, err := models.NewCities(mockReader)
			if err != nil {
				t.Fatal("Error creating cities", err)
			}
			result := cities.Filter(tt.query)
			fmt.Printf("####result %d", len(result))
			if len(result) != len(tt.want) {
				t.Fatalf("Expected %v, got %v", len(tt.want), len(result))
			}
			for i, w := range tt.want {
				if result[i].Name() != w.Name {
					t.Errorf("result[%v] expected %v but got %v", i, w, result[i])
				}
			}
		})
	}
}

func skiReady() server.RespCities {
	return server.RespCities{
		Id:          1,
		Name:        "Tirana",
		HasBeach:    false,
		HasMountain: true,
		Temperature: []server.Temps{
			{Id: 1, CityId: 1, Value: 13},
			{Id: 1, CityId: 1, Value: 3},
			{Id: 1, CityId: 1, Value: -3},
		},
	}
}

func beachReady1() server.RespCities {
	return server.RespCities{
		Id:          2,
		Name:        "Genova",
		HasBeach:    true,
		HasMountain: false,
		Temperature: []server.Temps{
			{Id: 1, CityId: 2, Value: 23},
			{Id: 2, CityId: 2, Value: 24},
			{Id: 3, CityId: 2, Value: 25},
		},
	}
}

func beachReady2() server.RespCities {
	return server.RespCities{
		Id:          3,
		Name:        "Rimini",
		HasBeach:    true,
		HasMountain: false,
		Temperature: []server.Temps{
			{Id: 4, CityId: 3, Value: 21},
			{Id: 5, CityId: 3, Value: 24},
		},
	}
}
