### To run the project as standalone just do: 
1.    go build .
2.    ./tempService -month 3 -ski

### Run the project form the terminal :
1. go run main.go -month 3 -ski

### To Run the tests project just do from the root dir of the project:
1. go test ./... -v
2. to test a single function do this
3. go test -run TestGetNonExsistentCity ./server/ -v

### To generate the mock stubs just run from the root project file:
1. go generate ./...

### To run the api just run:

1. go to myserver.go
2. rename main2 function into main
3. go to main.go and rename main function into main2
4. then run: go run myserver.go
5. then import the workspace from the curl folder into the postman ide
