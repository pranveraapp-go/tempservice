package main

import "tempService/server"

func main() {
	a := server.App{}
	a.Port = ":9003"
	a.Initialize()
	a.Run()
}
